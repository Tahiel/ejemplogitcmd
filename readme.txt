Ejemplo de como utilizar el Git y algunos comandos

Por primera vez utilizar el comando git clone

git clone https://gitlab.com/Tahiel/ejemplogitcmd.git 

Consulta

git status

Agregar uno o varios archivos al repositorio("." es un comodin)

git add .(archivo)

Mensaje que aparece al consultar el repositorio 

git commit --am "Mensaje de lo realizado en el proyecto"

Subir lo realizado al servidor

git push origin master

Una vez que ya clone, cotidianamente utilizo primero el comando 

git pull origin master